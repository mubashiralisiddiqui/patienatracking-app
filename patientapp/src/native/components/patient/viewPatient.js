import React, { Component } from 'react';
import { Container, Content, List, ListItem, Thumbnail, Text, Body, View, Icon } from 'native-base';
import { AsyncStorage, FlatList, ScrollView, TextInput } from 'react-native';
import DatePicker from 'react-native-datepicker'
import axios from 'axios';

import styles from './styles'
var allPatients = [];

export default class ViewPatient extends Component {
    constructor() {
        super();
        this.state = {
            filterPatient: [],
            data: [],
            filterText: '',
            filter: false,
            date: ''
        }
    }
    static navigationOptions = {
        title: 'View PAtient Details',
        headerTitleStyle: {
            color: 'white',
            fontFamily: 'Courier New',
            fontWeight: 'bold',
            fontSize: 20,
            justifyContent: 'space-between',
            textAlign: 'center',
            paddingLeft: 60
        },
        headerStyle: {
            backgroundColor: 'darkblue'
        }
    };
    componentDidMount() {
        this.viewPatient();
    }
    viewPatient() {
        allPatients = [];
        let Mydata;
        axios.get("https://patientappproject.herokuapp.com/api/allPatient")
            .then((res) => {
                allPatients = this.state.data
                allPatients = res.data.data
                this.setState({
                    data: allPatients
                })
            })
            .catch((err) => {
                console.log(err)
            })
    }
    searchByDate(date) {
        this.setState({
            date: date
        })
        var filterdata = this.state.data;
        this.setState({ date: date })
        var filterdata = filterdata.filter(asset => asset.date.indexOf(date) !== -1);
        this.setState({
            filter: true,
            filterPatient: filterdata
        })
    }
    searchByName(text) {
        var filterdata = this.state.data;
        this.setState({ filterText: text })
        var filterdata = filterdata.filter((data) => data.patientName.toLowerCase().indexOf(text) !== -1);
        this.setState({
            filter: true,
            filterPatient: filterdata
        })
    }
    render() {
        return (
            <List>
                <ListItem>
                    <Body>
                        <TextInput multiline={true}
                            style={styles.search}
                            onChangeText={(text) => this.searchByName(text.toLowerCase())}
                            placeholder="Search"
                            underlineColorAndroid='blue'
                            />
                        <DatePicker
                            style={{ width: 200 }}
                            date={this.state.date}
                            mode="date"
                            placeholder="select date"
                            format="D/M/YYYY"
                            minDate="1/1/2015"
                            maxDate="1/1/2018"
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            customStyles={{
                                dateIcon: styles.date,
                                dateInput: {
                                    marginLeft: 36
                                }
                            }}
                            onDateChange={(date) => this.searchByDate(date)}
                            />
                        <Text style={{ textAlign: 'center', color: 'blue', fontSize: 25, fontWeight: 'bold' }}>Patients</Text>
                        <ScrollView>
                            {this.state.filter ?
                                <FlatList
                                    data={this.state.filterPatient}
                                    renderItem={({ item, index }) =>
                                        <View key={index} style={styles.patientList}>
                                            <Content>
                                                <Icon name='person' />
                                                <Text style={{ marginTop: 10 }}>Date: {item.date}</Text>
                                                <Text >Name:  {item.patientName} </Text>
                                                <Text>Gender: {item.gender}</Text>
                                                <Text>Age: {item.patientAge}</Text>
                                                <Text>Desease: {item.disease}</Text>
                                                <Text>Description: {item.description}</Text>
                                            </Content>
                                        </View>
                                    }
                                    keyExtractor={item => item.id}
                                    /> :
                                <FlatList
                                    data={this.state.data}

                                    renderItem={({ item, index }) =>
                                        <ScrollView key={item.id} contentContainerStyle={styles.patientList}>
                                            <Content>
                                                <Icon name='person' />
                                                <Text style={{ marginTop: 10 }}>Date: {item.date}</Text>
                                                <Text >Name:  {item.patientName} </Text>
                                                <Text>Gender: {item.gender}</Text>
                                                <Text>Age: {item.patientAge}</Text>
                                                <Text>Desease: {item.disease}</Text>
                                            </Content>
                                        </ScrollView>
                                    }
                                    keyExtractor={item => item.id}
                                    />}
                        </ScrollView>
                    </Body>
                </ListItem>
            </List>
        )
    }
}
