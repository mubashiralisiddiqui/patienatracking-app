import Home from './home/home';
import AddPatient from './patient/addPatient'
import ViewPatient from './patient/viewPatient';
   
export {
    Home,
    AddPatient,
    ViewPatient
}