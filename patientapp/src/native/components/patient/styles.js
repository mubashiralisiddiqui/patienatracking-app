import React from 'react'
import { StyleSheet } from 'react-native';
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    Content: {
        width: 300,
        height: 100
    },
    addPatient: {
        textAlign: 'center',
        marginTop: 50
    },
    textAreaAddPatient: {
        width: 300,
        height: 100,
        borderWidth: 2,
        borderColor: 'gray',
        marginTop: 20,
        marginBottom: 10
    },
    buttonContainer: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
    },
    button: {
        backgroundColor: 'skyblue',
        borderRadius: 10,
    },
    patientList: {
        borderBottomWidth: 1,
        borderBottomColor: 'gray',
        marginTop: 10,
    },
    date: {
        position: 'absolute',
        left: 0,
        top: 4,
        marginLeft: 0
    },
search:{
     height: 40,
}


});
export default styles;